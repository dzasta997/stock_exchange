import java.util.*;

public class Stock {
    private HashMap<Integer, Order> orders;
    private LinkedList<OrderData> orderData;
    private Integer lastId;

    public Stock() {
        this.orders = new HashMap<>();
        this.orderData = new LinkedList<>();
        this.lastId = 0;
    }

    public Integer nextId(){
        return ++ lastId;
    }

    public void addOrder(Order order){
        int id = nextId();
        orders.put(id, order);
        addOrderData(order, id, "Add");
        showStep();
    }

    public void removeOrder(int id){
        if(orders.containsKey(id)){
            addOrderData(orders.get(id), id, "Remove");
            orders.remove(id);
            showStep();
        }
    }

    public void addOrderData(Order order, int id, String type){
        orderData.add(new OrderData(order, id, type));
    }

    public void showStep(){
        showBestSell();
        showBestBuy();
        showOrderData();
        System.out.println("\n--------------------------------------------------\n\n");
    }
    public void showBestSell(){
        bestSell().ifPresentOrElse(
                o -> System.out.printf(
                        "Best sell: %s Price: %s Quantity: %s\n",
                        String.format("%03d",o.getKey()), o.getValue().getPrice(), o.getValue().getQuantity()),
                () -> System.out.println("No selling offer")
        );
    }
    public void showBestBuy(){
        bestBuy().ifPresentOrElse(
                o -> System.out.printf(
                        "Best buy: %s Price: %s Quantity: %s\n",
                        String.format("%03d",o.getKey()), o.getValue().getPrice(), o.getValue().getQuantity()),
                () -> System.out.println("No buying offer")
        );
    }
    public void showOrderData(){
        System.out.printf("\n%5s %10s %10s %10s %10s\n","Id", "Order", "Type", "Price", "Quantity");
        orderData.stream().forEach(
                o -> System.out.printf("%5s %10s %10s %10s %10s\n", String.format("%03d",o.getId()), o.getOrder(), o.getType(), o.getPrice(), o.getQuantity())
        );
    }

    public Optional<Map.Entry<Integer, Order>> bestSell(){
        return orders.entrySet()
                .stream()
                .filter(o -> o.getValue().getOrder().equals("Sell"))
                .min(Comparator.comparing(o -> o.getValue().getPrice()/o.getValue().getQuantity()));
    }

    public Optional<Map.Entry<Integer, Order>> bestBuy(){
        return orders.entrySet()
                .stream()
                .filter(o -> o.getValue().getOrder().equals("Buy"))
                .max(Comparator.comparing(o -> o.getValue().getPrice()/o.getValue().getQuantity()));
    }

    public HashMap<Integer, Order> getOrders() {
        return orders;
    }

    public void setOrders(HashMap<Integer, Order> orders) {
        this.orders = orders;
    }

    public LinkedList<OrderData> getOrderData() {
        return orderData;
    }

    public void setOrderData(LinkedList<OrderData> orderData) {
        this.orderData = orderData;
    }

    public Integer getLastId() {
        return lastId;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }

}
