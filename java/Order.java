public class Order {

    private String order;
    private double price;
    private int quantity;

    public Order(String order, double price, int quantity){
        this.order = order;
        this.price = price;
        this.quantity = quantity;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Order{" +
                "order='" + order + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
