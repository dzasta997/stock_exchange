import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Stock stock = new Stock();
        stock.addOrder(new Order("Buy", 20.0, 100));
        stock.addOrder(new Order("Sell", 25.0, 200));
        stock.addOrder(new Order("Buy", 23.0, 50));
        stock.addOrder(new Order("Buy", 20.0, 70));
        stock.removeOrder(3);
        stock.addOrder(new Order("Sell", 28.0, 100));

    }
}
