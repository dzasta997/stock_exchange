public class OrderData {

    private int id;
    private String order;
    private String type;
    private double price;
    private int quantity;

    public OrderData(Order order, int id, String type ) {
        this.id = id;
        this.type = type;
        this.order = order.getOrder();
        this.price = order.getPrice();
        this.quantity = order.getQuantity();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
